# 渐变色进度条

**演示**: https://ricky1986.gitee.io/progress_bar/

演示中使用了layui和jQuery

## 参与

- 包含 [Thibaut Courouble](http://thibaut.me) 的编码

- Original PSD by [Vin Thomas](http://365psd.com/day/2-117/).



## 协议

Copyright (c) 2012-2013 Thibaut Courouble

Licensed under the MIT License
